#!/bin/bash

docker run \
	-it \
	--rm \
	--env DISPLAY=$DISPLAY \
	--volume /tmp/.X11-unix:/tmp/.X11-unix \
	--volume $(pwd):$(pwd) \
	--workdir $(pwd) \
	--env HOST_USER_ID=$UID \
	--env HOST_GROUP_ID=$(id -g $USER) \
	--hostname build \
	mario:default

