#!/bin/bash -e

boost_file="${1}"

echo "boost"

cd /tmp
tar -xf boost_${boost_file}.tar.bz2
rm boost_${boost_file}.tar.bz2

cd boost_${boost_file}
./bootstrap.sh --prefix=${INSTALL_PREFIX} --with-libraries=regex,system,program_options
./b2 -j $(nproc) --prefix=${INSTALL_PREFIX} threading=multi variant=release optimization=space install

rm -fr /tmp/boost_${boost_file}
