#!/bin/bash -e

## https://github.com/jbeder/yaml-cpp
#RUN echo "yaml-cpp" \
#	&& cd /opt \
#	&& mkdir tmp \
#	&& cd tmp \
#	&& git clone https://github.com/jbeder/yaml-cpp \
#	&& cd yaml-cpp \
#	&& git checkout 86ae3a5aa7e2109d849b2df89176d6432a35265d \
#	&& cd .. \
#	&& mkdir build \
#	&& cd build \
#	&& cmake ../yaml-cpp -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} -DYAML_CPP_BUILD_TESTS=NO -DYAML_CPP_BUILD_TOOLS=NO -DYAML_CPP_BUILD_CONTRIB=NO -DBUILD_SHARED_LIBS=NO \
#	&& make -j2 \
#	&& make install \
#	&& cd /opt \
#	&& rm -fr tmp

