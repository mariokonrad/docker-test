#!/bin/bash -e

nlohmann_json=${1}

echo "nlohmann-json"

if [ ! -d ${INSTALL_PREFIX}/include/nlohmann ] ; then
	mkdir -p ${INSTALL_PREFIX}/include/nlohmann
fi

mv /tmp/json.hpp ${INSTALL_PREFIX}/include/nlohmann

