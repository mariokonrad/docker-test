#!/bin/bash -e

googletest="${1}"

echo "googletest"

cd /tmp
tar -xf release-${googletest}.tar.gz
rm -r release-${googletest}.tar.gz

mkdir -p /tmp/build-googletest
cd /tmp/build-googletest
cmake /tmp/googletest-release-${googletest} -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX}
make -j $(nproc)
make install

rm -fr /tmp/googletest-release-${googletest}
rm -fr /tmp/build-googletest
