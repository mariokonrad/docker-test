#!/bin/bash -e

fmt=${1}

echo "fmt"

cd /tmp
git clone https://github.com/fmtlib/fmt
cd fmt
git checkout ${fmt}

mkdir -p /tmp/build-fmt
cd /tmp/build-fmt
cmake /tmp/fmt \
	-DCMAKE_BUILD_TYPE=Release \
	-DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
	-DFMT_DOC=NO \
	-DFMT_TEST=NO \
	-DFMT_USE_CPP14=YES
make -j $(nproc)
make install

rm -fr /tmp/fmt
rm -fr /tmp/build-fmt

