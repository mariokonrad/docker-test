#!/bin/bash -e

cmake_file=${1}

echo "cmake"

cd /tmp
tar -xf cmake-${cmake_file}-Linux-x86_64.tar.gz -C /opt/
rm -f cmake-${cmake_file}-Linux-x86_64.tar.gz

ln -s /opt/cmake-${cmake_file}-Linux-x86_64/bin/cmake /usr/local/bin/cmake
ln -s /opt/cmake-${cmake_file}-Linux-x86_64/bin/cpack /usr/local/bin/cpack
ln -s /opt/cmake-${cmake_file}-Linux-x86_64/bin/ctest /usr/local/bin/ctest

