FROM gcc:7.3.0

LABEL maintainer="Mario Konrad <mario.konrad@gmx.net>"

ENV cmake_dir=3.11
ENV cmake_file=3.11.1

ENV boost_dir=1.67.0
ENV boost_file=1_67_0

ENV googletest=1.8.0

ENV fmt=4.1.0

ENV nlohmann_json=3.1.2

ENV INSTALL_PREFIX=/opt/local
ENV BOOST_ROOT=${INSTALL_PREFIX}

RUN apt-get update \
	&& apt-get install -y apt-utils wget git \
	&& rm -fr /var/lib/apt/lists/*

RUN mkdir -m 0777 -p /opt

COPY dlcache/cmake-${cmake_file}-Linux-x86_64.tar.gz /tmp/
COPY build.d/cmake.sh /tmp/
RUN /tmp/cmake.sh "${cmake_file}"

COPY dlcache/boost_${boost_file}.tar.bz2 /tmp/
COPY build.d/boost.sh /tmp/
RUN /tmp/boost.sh "${boost_file}"

COPY dlcache/release-${googletest}.tar.gz /tmp/
COPY build.d/googletest.sh /tmp/
RUN /tmp/googletest.sh "${googletest}"

COPY build.d/fmt.sh /tmp/
RUN /tmp/fmt.sh "${fmt}"

COPY dlcache/json.hpp /tmp/
COPY build.d/nlohmann_json.sh /tmp/
RUN /tmp/nlohmann_json.sh "${nlohmann_json}"

RUN echo "alias l='ls -l'" >> /etc/bash.bashrc

ADD init-user.sh /usr/local/bin/init-user.sh
RUN chmod a+x /usr/local/bin/init-user.sh
ENTRYPOINT ["init-user.sh"]
