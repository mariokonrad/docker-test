#!/bin/bash -e

# check for the existence of HOST_GROUP_ID and HOST_USER_ID
if [ "x$HOST_GROUP_ID" == "x" ] ; then
	echo "error: HOST_GROUP_ID not set"
	exit -1
fi
if [ "x$HOST_USER_ID" == "x" ] ; then
	echo "error: HOST_USER_ID not set"
	exit -1
fi

HOST_GROUP_NAME=users
HOST_USER_NAME=user
USER_SHELL="/bin/bash"
GROUP_NAME=$HOST_GROUP_NAME

# check group
if ! grep $HOST_GROUP_ID /etc/group 1>/dev/null 2>/dev/null ; then
	echo "info: group ID does not exist: $HOST_GROUP_ID"
	if ! grep $HOST_GROUP_NAME /etc/group 1>/dev/null 2>/dev/null ; then
		echo "info: group NAME does not exist: $HOST_GROUP_NAME"
		echo "info: create group $HOST_GROUP_NAME with GID $HOST_GROUP_ID"
		groupadd -g $HOST_GROUP_ID -r $HOST_GROUP_NAME
	else
		echo "info: group NAME already exists: $HOST_GROUP_NAME"
		echo "info: create group with same name as user: $HOST_USER_NAME"
		GROUP_NAME=$HOST_USER_NAME
		groupadd -g $HOST_GROUP_ID -r $HOST_USER_NAME
	fi
else
	echo "info: group ID already exists: $HOST_GROUP_ID"
fi

# check user
if ! grep $HOST_USER_ID /etc/passwd 1>/dev/null 2>/dev/null ; then
	echo "info: user ID does not exist: $HOST_USER_ID"
	if ! grep $HOST_USER_NAME /etc/passwd 1>/dev/null 2>/dev/null ; then
		echo "info: user NAME does not exist: $HOST_USER_NAME"
		echo "info: create user $HOST_USER_NAME with UID $HOST_USER_ID"
		useradd -m -s $USER_SHELL -u $HOST_USER_ID -g $GROUP_NAME $HOST_USER_NAME
	else
		echo "info: user NAME already exists: $HOST_USER_NAME"
	fi
else
	echo "info: user ID already exists: $HOST_USER_ID"
fi

# finishing
/bin/su -s $USER_SHELL $HOST_USER_NAME
