#!/bin/bash

# clean up stopped containers
if [ "x$(docker ps -q -f status=exited)" != "x" ] ; then
	docker rm $(docker ps -q -f status=exited)
fi

# clean up orphaned and untagged images
if [ "x$(docker images -q -f dangling=true)" != "x" ] ; then
	docker rmi $(docker images -q -f dangling=true)
fi

# clean up orphaned volumed
if [ "x$(docker volume ls -qf dangling=true)" != "x" ] ; then
	docker volume rm $(docker volume ls -qf dangling=true)
fi
