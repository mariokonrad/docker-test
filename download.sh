#!/bin/bash -e

cmake_dir=3.11
cmake_file=3.11.1

boost_dir=1.67.0
boost_file=1_67_0

googletest=1.8.0

fmt=4.1.0

nlohmann_json=3.1.2

function download_cache()
{
	url=${1}
	filename=${2}

	echo "check ${filename}"

	if [ ! -d dlcache ] ; then
		mkdir -p dlcache
	fi

	if [ ! -r dlcache/${filename} ] ; then
		(cd dlcache ; wget ${url})
	fi
}

download_cache \
	https://cmake.org/files/v${cmake_dir}/cmake-${cmake_file}-Linux-x86_64.tar.gz \
	cmake-${cmake_file}-Linux-x86_64.tar.gz

download_cache \
	https://dl.bintray.com/boostorg/release/${boost_dir}/source/boost_${boost_file}.tar.bz2 \
	boost_${boost_file}.tar.bz2

download_cache \
	https://github.com/google/googletest/archive/release-${googletest}.tar.gz \
	release-${googletest}.tar.gz

#download_cache \
#	https://github.com/fmtlib/fmt/archive/${fmt}.tar.gz \
#	${fmt}.tar.gz

download_cache \
	https://github.com/nlohmann/json/releases/download/v${nlohmann_json}/json.hpp \
	json.hpp

